# Marvel Project

This project gets the comic list from Marvel API and the detail of them. Also lets us save our favorite comics after we signup and login.

## Requirements

* python 3.7 +
* SQLite DataBase

## Installation

* Create env vars:
    * API_KEY_MARVEL
    * API_PRIVATE_KEY_MARVEL
    * API_URL_MARVEL
    * API_PORT_MARVEL

* Install requirements:
    ```sh
    pip install -r requirements.txt
    ```

* Make sure you have the "db.sqlite3" file in the project's root.

* Run migrations:
    ```sh
    python manage.py migrate
    ```

## Run the project
* Make sure you are in the project's root and execute:
    ```sh
        python manage.py runserver
    ```
* Open the next URL in  your browser: http://127.0.0.1:8000/


# Design Patter Implemented

For this project was used the Proxy Design Pattern. A proxy class was implemented to comunicate our local project with the Marvel API by getting the comics list and  their detail information. There is a litle explanation about this Pattern Design in a repository of my personal gitlab account:
https://gitlab.com/charlesoliveros/pydesignpatterns/-/blob/master/2_structural/proxy.py