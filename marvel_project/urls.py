from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'', include('apps.main.urls', )),
    path(r'', include('apps.comics.urls', )),
    path(r'', include('apps.userprofile.urls', )),
]
