from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import comics, comics_favorites, comic_detail, comic_favorite_create

app_name = 'comics'

urlpatterns = [
    path('comics/', login_required(comics), name='comics'),
    path('comics/<int:comic_id>', login_required(comic_detail), name='comic_detail'),
    path('comics/favorites/', login_required(comics_favorites), name='comics_favorites'),
    path(
        'comics/favorites/create/<int:comic_id>',
        login_required(comic_favorite_create),
        name='comic_favorite_create'
    ),
]
