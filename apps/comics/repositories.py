from django.contrib import messages
from apps.main.repositories import RepositoryAbstract
from .api_proxy import APIProxyMarvelV1
from .models import FavoriteComic


class ComicRepository(RepositoryAbstract):
    '''Repository to get, create and update all the Comics data.
    '''
    api_proxy_marvel_v1: APIProxyMarvelV1

    def __init__(self) -> None:
        '''
        api_proxy_marvel_v1 is the Proxy to get all the comics from API MARVEL
        '''
        self.api_proxy_marvel_v1 = APIProxyMarvelV1()

    def get_comics(self, request) -> tuple:
        '''
        GET comics by using the proxy object
        '''
        queryparams = {'offset': request.GET.get('offset')}
        response = self.api_proxy_marvel_v1.get_comics(queryparams)
        return self.get_response(response)

    def get_comic(self, comic_id: int) -> tuple:
        response = self.api_proxy_marvel_v1.get_comic(comic_id)
        return self.get_response(response)

    def get_comics_favorites(self, request) -> tuple:
        '''
        GET favorite comics by using the ORM Model
        '''
        comics_favorites = FavoriteComic.objects.filter(user=request.user).order_by('-id')
        return comics_favorites

    def create_or_update_favorite_comic(self, request, comic_id: int) -> bool:
        comics, status_code = self.get_comic(comic_id)

        if comics and status_code == 200:
            if comics.get('data') and comics.get('data').get('results'):
                comic_original = comics.get('data').get('results')[0]
                comic_favorite = FavoriteComic.objects.filter(
                    user=request.user, comic_id=comic_id
                ).first()

                if not comic_favorite:
                    comic_favorite = FavoriteComic()

                comic_favorite.user = request.user
                comic_favorite.comic_id = comic_original.get('id')
                comic_favorite.title = comic_original.get('title')
                comic_favorite.description = comic_original.get('description')
                comic_favorite.image_url = (
                    f"{comic_original.get('thumbnail').get('path')}."
                    f"{comic_original.get('thumbnail').get('extension')}"
                )
                comic_favorite.save()
                messages.info(request, 'Comic was saved in favorites!')
                return True

        return False
