from typing import Union
import logging
import hashlib
from datetime import datetime
from django.conf import settings
import requests
from apps.main.api_proxy import APIProxyAbstract

logger = logging.getLogger(__name__)


class APIProxyMarvelV1(APIProxyAbstract):
    '''Proxy class to get the data from the MARVEL API
    '''

    def __init__(self) -> None:
        self.api_url = settings.API_URL_MARVEL
        self.api_key = settings.API_KEY_MARVEL
        self.api_private_key = settings.API_PRIVATE_KEY_MARVEL
        self.api_port = settings.API_PORT_MARVEL

    def get_url(self, endpoint: str, queryparams: dict = None) -> str:
        '''Create the hash required by Marvel API
        '''
        hash_md5 = hashlib.md5()
        ts = str(datetime.now().timestamp())
        hash_md5.update(ts.encode())
        hash_md5.update(self.api_private_key.encode())
        hash_md5.update(self.api_key.encode())
        marvel_hash = hash_md5.hexdigest()
        url = (
            f'{self.api_url}:{self.api_port}/{endpoint}'
            f'?apikey={self.api_key}&hash={marvel_hash}&ts={ts}'
        )
        if queryparams:
            for name, value in queryparams.items():
                if name and value is not None:
                    url += f'&{name}={value}'
        return url

    def send_request(
        self,
        request_method: Union[requests.get, requests.post],
        endpoint: str,
        queryparams: dict = None,
        data: dict = None
    ) -> requests.Response:
        url = self.get_url(endpoint, queryparams)
        response = request_method(url=url, data=data)
        return response

    def get_comics(self, queryparams: dict = None) -> requests.Response:
        '''GET Comic Request
        '''
        ENDPOINT = 'v1/public/comics'
        response: requests.Response = None

        try:
            response = self.send_request(
                request_method=requests.get, endpoint=ENDPOINT, queryparams=queryparams
            )
        except Exception as err:
            logger.error(err)
        return response

    def get_comic(self, comic: int) -> requests.Response:
        '''GET Comic Request
        '''
        ENDPOINT = f'v1/public/comics/{comic}'
        response: requests.Response = None

        try:
            response = self.send_request(request_method=requests.get, endpoint=ENDPOINT)
        except Exception as err:
            logger.error(err)
        return response
