from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from .repositories import ComicRepository


@require_http_methods(['GET', ])
def comics(request):
    comic_repository = ComicRepository()
    comics, status_code = comic_repository.get_comics(request)
    context = {'comics': comics}
    return render(request, 'comics/index.html', context, status=status_code)


@require_http_methods(['GET', ])
def comic_detail(request, comic_id: int):
    comic_repository = ComicRepository()
    comic, status_code = comic_repository.get_comic(comic_id)
    context = {'comic': comic}
    return render(request, 'comics/detail.html', context, status=status_code)


@require_http_methods(['GET', ])
def comics_favorites(request):
    comic_repository = ComicRepository()
    comics = comic_repository.get_comics_favorites(request)
    context = {'comics': comics}
    return render(request, 'comics/favorites.html', context)


@require_http_methods(['GET', ])
def comic_favorite_create(request, comic_id: int):
    comic_repository = ComicRepository()
    comic_repository.create_or_update_favorite_comic(request, comic_id)
    return redirect('/comics/favorites/')
