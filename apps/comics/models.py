from django.db import models
from django.contrib.auth import get_user_model


class FavoriteComic(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    comic_id = models.IntegerField()
    title = models.CharField(max_length=400)
    description = models.TextField()
    image_url = models.URLField(max_length=400)

    class Meta:
        unique_together = ('user', 'comic_id',)
