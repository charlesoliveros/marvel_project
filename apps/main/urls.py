from django.urls import path
# from django.contrib.auth.decorators import login_required
from .views import home, login, logout

app_name = 'main'

urlpatterns = [
    path('', home, name='home'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
]
