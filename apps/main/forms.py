from django import forms


class UserRegisterForm(forms.Form):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    identification = forms.CharField(max_length=20)
    email = forms.EmailField()
    email_confirm = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    password_confirm = forms.CharField(widget=forms.PasswordInput())

    def clean(self) -> dict:
        cleaned_data = super().clean()
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('password_confirm')
        email = cleaned_data.get('email')
        email_confirm = cleaned_data.get('email_confirm')

        if password != password_confirm:
            # raise forms.ValidationError('las contraseñas son diferentes')
            self.add_error('password', 'Passwords are differents')
            self.add_error('password_confirm', 'Passwords are differents')

        if email != email_confirm:
            self.add_error('email', 'Emails are differents')
            self.add_error('email_confirm', 'Emails are differents')

        return cleaned_data


class LoginForm(forms.Form):
    email = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            {
                'placeholder': 'Email',
                'class': 'contactus'
            }
        )
    )
    password = forms.CharField(
        max_length=100,
        widget=forms.PasswordInput(
            {
                'placeholder': 'Password',
                'class': 'contactus'
            }
        )
    )
