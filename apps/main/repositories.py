from abc import ABC, abstractmethod
import requests


class RepositoryAbstract(ABC):
    '''Abstract Repository to be implemented
    '''

    @abstractmethod
    def __init__(self) -> None:
        '''Here in the constructor implementation, the proxy class must be instanced
        '''
        pass

    def get_json(self, response: requests.Response) -> dict:
        if response is not None:
            return response.json()
        return {}

    def get_status_code(self, response: requests.Response) -> int:
        if response is not None:
            return response.status_code
        return None

    def get_response(self, response: requests.Response) -> tuple:
        return self.get_json(response), self.get_status_code(response)
