from abc import ABC, abstractmethod
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from rest_framework.status import HTTP_401_UNAUTHORIZED

from .forms import LoginForm
from .error_catalogue import USER_IS_NOT_VALID
import logging

logger = logging.getLogger(__name__)


class AuthenticationAbstract(ABC):
    '''
    Inverson Dependency Principle
    '''

    @abstractmethod
    def login_user(self) -> tuple:
        pass

    @abstractmethod
    def logout_user(self) -> None:
        pass


class Authentication(AuthenticationAbstract):

    @staticmethod
    def login_user(request, form: LoginForm) -> tuple:
        user = None
        status_code: str = None
        username: str = form.cleaned_data['email']
        password: str = form.cleaned_data['password']
        login_is_valid: bool = False

        try:
            user = authenticate(
                username=username,
                password=password
            )
        except Exception as err:
            logger.error(err)

        if user and user.is_active:
            login(request, user)
            login_is_valid = True

        if not login_is_valid:
            messages.error(request, USER_IS_NOT_VALID)
            status_code = HTTP_401_UNAUTHORIZED

        return login_is_valid, status_code

    @staticmethod
    def logout_user(request) -> None:
        try:
            logout(request)
        except Exception as err:
            logger.error(err)
