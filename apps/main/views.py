from django.shortcuts import render, redirect
from .auth import Authentication
from .forms import LoginForm


def home(request):
    return render(request, 'base/index.html', {})


def login(request):
    if request.user.is_authenticated:
        return redirect('/comics/')

    context = {}
    form = LoginForm()
    status_code: str = None

    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid():
            login_is_valid, status_code = Authentication.login_user(request, form)

            if login_is_valid:
                return redirect('/comics/')

    context['form'] = form
    return render(request, 'main/login.html', context, status=status_code)


def logout(request):
    Authentication.logout_user(request)
    return redirect('/')
