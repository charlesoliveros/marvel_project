from abc import ABC, abstractmethod
import requests


class APIProxyAbstract(ABC):
    '''
    Proxy Pattern Design.
    Proxy class to be implemented for every needed API
    '''

    api_url: str = None
    api_key: str = None
    api_port: str = None

    @abstractmethod
    def __init__(self) -> None:
        '''
        This contructor must be implemented to get the URL, token and API Port
        from the django settings
        '''
        pass

    @abstractmethod
    def send_request(self) -> requests.Response:
        '''
        This method must be implemented by implementing the API requements
        '''
        pass
