from django.urls import path
from .views import signup

app_name = 'userprofile'

urlpatterns = [
    path('signup/', signup, name='signup'),
]
