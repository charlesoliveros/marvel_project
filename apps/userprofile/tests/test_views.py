from http import HTTPStatus
from django.test import TestCase


class UserProfileIntegrationTest(TestCase):

    def test_singup_when_is_ok(self):
        '''
        Test signup when the user is created ok
        '''
        data = {
            'first_name': 'Carlos',
            'last_name': 'Oliveros',
            'identification': 111222333,
            'email': 'carlos@email.com',
            'email_confirm': 'carlos@email.com',
            'password': '123',
            'password_confirm': '123',
        }
        response = self.client.post("/signup/", data=data)

        self.assertEqual(response.status_code, HTTPStatus.CREATED)

    def test_singup_when_user_already_exist(self):
        '''
        Test signup when the user already exists
        '''
        data = {
            'first_name': 'Carlos',
            'last_name': 'Oliveros',
            'identification': 111222333,
            'email': 'carlos@email.com',
            'email_confirm': 'carlos@email.com',
            'password': '123',
            'password_confirm': '123',
        }

        self.client.post("/signup/", data=data)
        response = self.client.post("/signup/", data=data)

        self.assertEqual(response.status_code, HTTPStatus.CONFLICT)
