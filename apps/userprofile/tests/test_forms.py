from apps.userprofile.forms import UserRegisterForm
from django.test import TestCase


class UserProfileFormsUnitTest(TestCase):

    def test_register_form_when_emails_are_differents(self):
        form = UserRegisterForm(
            data={'email': 'carlos@email.com', 'email_confirm': 'andres@email.com'}
        )
        self.assertEqual(form.errors['email'], ['Emails are differents'])

    def test_register_form_when_passwords_are_differents(self):
        form = UserRegisterForm(
            data={'password': '111', 'password_confirm': '222'}
        )
        self.assertEqual(form.errors['password'], ['Passwords are differents'])
