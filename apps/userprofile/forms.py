from django import forms


class UserRegisterForm(forms.Form):
    first_name = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            {
                'placeholder': 'First Name',
                'class': 'contactus'
            }
        )
    )
    last_name = forms.CharField(
        max_length=100,
        widget=forms.TextInput(
            {
                'placeholder': 'Last Name',
                'class': 'contactus'
            }
        )
    )
    identification = forms.CharField(
        max_length=20,
        widget=forms.TextInput(
            {
                'placeholder': 'Identification',
                'class': 'contactus'
            }
        )
    )
    email = forms.EmailField(
        widget=forms.TextInput(
            {
                'placeholder': 'Email',
                'class': 'contactus'
            }
        ))
    email_confirm = forms.EmailField(
        widget=forms.TextInput(
            {
                'placeholder': 'Email Confirm',
                'class': 'contactus'
            }
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            {
                'placeholder': 'Password',
                'class': 'contactus'
            }
        )
    )
    password_confirm = forms.CharField(
        widget=forms.PasswordInput(
            {
                'placeholder': 'Password Confirm',
                'class': 'contactus'
            }
        )
    )

    def clean(self) -> dict:
        cleaned_data = super().clean()
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('password_confirm')
        email = cleaned_data.get('email')
        email_confirm = cleaned_data.get('email_confirm')

        if password != password_confirm:
            # raise forms.ValidationError('las contraseñas son diferentes')
            self.add_error('password', 'Passwords are differents')
            self.add_error('password_confirm', 'Passwords are differents')

        if email != email_confirm:
            self.add_error('email', 'Emails are differents')
            self.add_error('email_confirm', 'Emails are differents')

        return cleaned_data
