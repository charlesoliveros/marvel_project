from abc import ABC, abstractmethod
import logging

from django.contrib.auth import get_user_model
from django.db import transaction, IntegrityError
from django.contrib import messages

from rest_framework.status import HTTP_409_CONFLICT, HTTP_400_BAD_REQUEST, HTTP_201_CREATED

from apps.main.error_catalogue import USER_EXISTS
from .models import UserProfile
from .forms import UserRegisterForm

logger = logging.getLogger(__name__)


class UserRegisterLogicAbstract(ABC):
    '''
    Dependency Inversion Principle
    '''

    @abstractmethod
    def signup(self) -> tuple:
        pass


class UserRegisterLogic(UserRegisterLogicAbstract):

    @classmethod
    def signup(cls, request, form: UserRegisterForm) -> tuple:
        user_register_is_valid: bool = False
        status_code: str = None

        try:
            cls.create_user(form)
        except IntegrityError:
            status_code = HTTP_409_CONFLICT
            messages.error(request, USER_EXISTS)
        except Exception as err:
            status_code = HTTP_400_BAD_REQUEST
            print(err)
            logger.error(err)
        else:
            user_register_is_valid = True
            status_code = HTTP_201_CREATED

        return user_register_is_valid, status_code

    @classmethod
    @transaction.atomic()
    def create_user(cls, form: UserRegisterForm) -> object:
        User = get_user_model()

        user = User.objects.create_user(
            username=form.cleaned_data.get('email'),
            email=form.cleaned_data.get('email'),
            first_name=form.cleaned_data.get('first_name'),
            last_name=form.cleaned_data.get('last_name'),
            password=form.cleaned_data.get('password'),
        )

        user.is_active = True
        user.save()

        UserProfile.objects.create(
            user=user, identification_number=form.cleaned_data.get('identification')
        )
        return user
