from django.shortcuts import render
from .forms import UserRegisterForm
from .business_logic import UserRegisterLogic


def signup(request):
    context = {}
    form = UserRegisterForm()
    status_code: str = None
    user_register_is_valid: bool = False

    if request.method == 'POST':
        form = UserRegisterForm(request.POST)

        if form.is_valid():
            user_register_is_valid, status_code = UserRegisterLogic.signup(request, form)
            context['user_register_is_valid'] = user_register_is_valid

    context['form'] = form
    return render(request, 'userprofile/signup.html', context, status=status_code)
