from django.db import models
from django.contrib.auth import get_user_model


class UserProfile(models.Model):
    '''
        This model lets us to apply the Open-Close Principle
    '''
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    identification_number = models.CharField(unique=True, max_length=100)
